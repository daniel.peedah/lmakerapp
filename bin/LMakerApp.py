import sys
import time
from datetime import datetime
import random
import os
import os.path
import re
import platform
from random import seed
from random import randint
import configparser

#Creating default input settings

#Replace defaults with current v in ini file
config = configparser.ConfigParser()
APPNAME = "LMakerApp"
theplatf = platform.system()
# checking for splunk home
placepref = "/opt"
placepref1 ="C:\Program Files"
s1 = ""
s2 = ""
if theplatf == "Linux":
	pass
else:
	placepref = placepref1

s1 = os.path.join(placepref,"splunkforwarder")
s2 = os.path.join(placepref,"splunk")

SPLUNK_HOME = "none"
if os.path.exists(s1):
	SPLUNK_HOME = s1
if os.path.exists(s2):
	SPLUNK_HOME = s2
#error and quit if there is no splunk home
if SPLUNK_HOME == "none":
	print("splunk home not identified, must be in opt as splunk or splunkforwarder")
	sys.exit(1)

# find directories, if not found, create them
folderc = os.path.join(SPLUNK_HOME,"etc","OgFiles")
foldern = os.path.join(SPLUNK_HOME,"etc","NewLogs")

if not os.path.exists(folderc):
	os.mkdir(folderc)

if not os.path.exists(foldern):
	os.mkdir(foldern)

pattern = "\d+"
spacePat = "\S+"
settingspaths = os.path.join(SPLUNK_HOME,"etc","apps",APPNAME,"bin","settings.ini")
if not os.path.exists(settingspaths):
	print("settings file not found")
else:
	print("settings found")
config.read(settingspaths)
settings = 'SETTINGS'

isRandom = config[settings].getBoolean['isBatch']

print(isRandom, timeRandom,timeFuture, futureMax)
settings = configurations["SETTINGS"]
isran = settings["isRandom"]
#! make other settings
print("Beginning script") # log to splunk
for root, dirs, files in os.walk(folderc):
	for name in files:
		if fileSkip :
			i = ranint(0,1000)
			if num%2 == 0 :
				continue	
		print(root)
		subdr = re.search(r'\bOgFiles\b(\S+)?', root)
		subdrr = subdr.group(0)
		sep = os.sep
		folders = subdrr.split(sep)
		print(folders)
		filename = root + os.sep + name
		namesplit = re.split(pattern,name)
		namesplit[0] = re.sub("\.\D+","",namesplit[0])
		# GET SUFFIX FILE TYPE AND APPEND TO NEW FILE NAME
		suffix = re.search("\.\w+",filename).group(0)
		newname = namesplit[0] + suffix
		if len(folders) == 2:
			newfile =  os.path.join(foldern,folders[1],newname)
			if not os.path.exists(os.path.join(foldern,folders[1])):
				os.mkdir(os.path.join(foldern,folders[1]))
		else:
			newfile = os.path.join(foldern,newname)
		print(newfile)
		currentFile = os.path.join(folderc,filename)
		onf = open(newfile,"w+")
		onf.close
		a = 0
		with open(currentFile,encoding="ISO-8859-1") as f: #encoding required for some characters to be parsed
			for line in f:
				a+=1
		b = round(a/48) #based off 1 day of data, we want 30 minutes worth 
		if b<1:
			b = 1
		oof = open(currentFile,encoding="ISO-8859-1")
		onf = open(newfile,"a")
		allLines = oof.readlines() #calculate time
		now = datetime.now()
		daystr = now.strftime("%d-%b-%Y ")
		timestr = now.strftime("%H:%M:%S")
		if a == 0 :
			continue
		v = 1
		for x in range(b):
			if isRandom==1:
				v = randint(1,a)
			aLine = allLines[v-1] # replace time in current event
			aLine = re.sub(r'\d{2}\-\D{3}\-\d{4}',daystr,aLine)
			aLine = re.sub(r'\d{4}\-\d{2}\-\d{2}',now.strftime("%Y-%m-%d),aLine)
			aLine = re.sub(r'\d{2}\-\d{2}\-\d{2}',now.strftime("%Y-%m-%d),aLine)

			aLine = re.sub(r'\d{2}\:\d+\:\d{2}(\.\d{3})?',timestr,aLine)
			if isRandom == 0 :
				v = v + 1;
			onf.write(aLine)
		oof.close()
		onf.close()
	print("Python script complete no err")
